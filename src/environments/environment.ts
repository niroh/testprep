// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyCrkQut6vK80IdwylnV2WImINZalhpLJ-o",
    authDomain: "classprep-e12df.firebaseapp.com",
    databaseURL: "https://classprep-e12df.firebaseio.com",
    projectId: "classprep-e12df",
    storageBucket: "classprep-e12df.appspot.com",
    messagingSenderId: "246294370328",
    appId: "1:246294370328:web:db97eff6333ec03f04cb6d",
    measurementId: "G-QPMMMVPN5D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
