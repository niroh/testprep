import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(private booksservice:BooksService,
              public authservice:AuthService) { }
  books$: Observable<any>;
  userId:string

  ngOnInit() {
    /* subscribing to the Observable via code
    this.books = this.booksservice.getBooks().subscribe(
      (books) => this.books = books
    );
  }
  */
 //this.books$ = this.booksservice.getBooks();
 //this.booksservice.addBooks();
 this.authservice.user.subscribe(
  user => {
    this.userId = user.uid;
    this.books$ = this.booksservice.getBooks(this.userId);
  }
)

  }

  deleteBook(id:string){
    this.booksservice.deleteBook(id, this.userId);
  }

  updateBook(userId:string, id:string, title:string, author:string){
    this.booksservice.updateBook(userId,id,title,author);
  }

}
