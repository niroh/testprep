import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';
import { Observable } from 'rxjs';
import { TempService } from '../temp.service';



@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(private activatedroute:ActivatedRoute,
              public tempservice:TempService) { }

  likes:number = 0;

  city:string;
  temp:number;
  image:String;
  errorMessage:string;
  hasError:Boolean = false;


  tempData$: Observable<Weather>;

  addLikes(){
    this.likes++;
  }


  ngOnInit() {
    this.city = this.activatedroute.snapshot.params['city'];
    //this.temp = this.activatedroute.snapshot.params['temp'];
    this.tempData$ = this.tempservice.searchWeatherDate(this.city);
    this.tempData$.subscribe(
      data => {
        this.image = data.image;
        this.temp = data.temperature;
      },

        error =>{
          this.hasError = true;
          this.errorMessage = error.message;
          
          console.log("in the component " + error.message);
        }
      
    )


  }

}
