import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  constructor(private router:Router) { }

  cities:object[] = [{city:'London'},{city:'Tel Aviv'},{city:'Rome'},{city:'Berlin'}, {city:'non-existent'}]
  tempInput:number;
  cityInput:string;

  ngOnInit() {
  }
  onSubmit(){
    this.router.navigate(['/temperatures', this.cityInput, this.tempInput])
  }

}
