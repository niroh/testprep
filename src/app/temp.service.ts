import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Weather } from './interfaces/weather';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL = "http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "1ba036447291baa09e1de9126d551d7a";
  private IMP = "&units=metric";

  constructor(private http:HttpClient) { }

  searchWeatherDate(cityName:String):Observable<Weather> {
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data=>this.transformWeatherData(data)),
          catchError(this.handleError))
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name: data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon,
    }   
  }
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error)
  }

}
