export interface WeatherRaw{ 
        coord:{ 
           lon:number,
           lat:number
        },
        weather:[ 
           { 
              description:String,
              icon:String
           }
        ];
        
        main:{ 
           temp:number,
        },
        sys:{
           country:String,
        },
        name:String,
     }
     

