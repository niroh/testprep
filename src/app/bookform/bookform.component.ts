import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  constructor(private bookservice:BooksService,
              public router:Router,
              private route:ActivatedRoute,
              private authservice:AuthService) {}
  id:string;
  title:string;
  author:string;

  isEdit:boolean = false;
  buttonText:string = "Add Book";
  userId:string;


  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id){
          this.isEdit = true;
          this.buttonText = "Update Book";
    
          this.bookservice.getBook(this.id, this.userId).subscribe(
            book => {
              this.author = book.data().author;
              this.title = book.data().title;
            }
          )
        }
      }
    )
    console.log(this.id);
    
    

  }

  onSubmit(){
    if(this.isEdit){
      this.bookservice.updateBook(this.userId,this.id,this.title,this.author);
    }
    else{
    this.bookservice.addBook(this.userId, this.title,this.author);
    }
    this.router.navigate(['/books']);
   
  }

}
